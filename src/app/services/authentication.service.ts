import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class AuthenticationService {
  constructor(private http: HttpClient) {
  }

  login(username: string, password: string) {
    const parms = new HttpParams().set('username', username).append('password', password);
    return this.http.post<any>('http://localhost:9000/authorization', {}, {params: parms})
      .pipe(map(bool => {
        // login successful if there's a user in the response
        if (bool) {
          localStorage.setItem('credentials', btoa(username + ':' + password));
        }
        return bool;
      }));
  }

  logout() {
    localStorage.removeItem('credentials');
  }
}
