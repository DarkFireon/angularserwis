import {Component, OnInit} from '@angular/core';
import {HttpService} from '../../services/Http.service';
import {Router} from '@angular/router';
import {DeviceInfo} from '../../app.component';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.css']
})
export class ManagementComponent implements OnInit {
  constructor(private httpService: HttpService, public router: Router) {
  }

  href: string;
  device: DeviceInfo;
  paramas: string[];
  param: string;
  value: string;
  comment: string;

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(100),
      distinctUntilChanged(),
      map(term => term.length < 1 ? []
        : this.paramas.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )

  ngOnInit(): void {
    this.href = this.router.url;
    console.log(this.href);
    this.getDevice(this.href);
    this.getParams();
  }

  refreshDeviceObject() {
    setTimeout(() => {
      this.getDevice(this.href);
    }, 350);
  }

  addCommentToDevice(deviceId: number, comment: string) {
    comment = comment.substr(0, 255);
    this.httpService.addCommentToDevice(deviceId, comment).subscribe();
    this.refreshDeviceObject();
    this.comment = '';
  }

  addParam(id: number, param: string, value: string) {
    this.httpService.addParam(id, param, value).subscribe();
    this.refreshDeviceObject();
    this.param = '';
    this.value = '';
    setTimeout(() => {
      this.getParams();
    }, 50);
  }

  repairDevice(id: number) {
    this.httpService.repairDevice(id).subscribe();
    this.refreshDeviceObject();
  }

  breakDevice(id: number) {
    this.httpService.breakDevice(id).subscribe();
    this.refreshDeviceObject();
  }

  deleteComment(id: number) {
    this.httpService.deleteComment(id).subscribe();
    this.refreshDeviceObject();
  }

  deleteParamFromDevice(deviceId: number, paramId: number) {
    this.httpService.deleteParamFromDevice(deviceId, paramId).subscribe();
    this.refreshDeviceObject();
  }

  getParams() {
    this.httpService.getParams().subscribe(paramas => {
      this.paramas = paramas;
    });
  }

  getDevice(href: string) {
    this.httpService.getDevice(href).subscribe(device => {
      this.device = device;
    });
  }
}
