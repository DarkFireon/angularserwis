import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ManagementComponent} from './components/management/management.component';
import {TableComponent} from './components/mainTable/table.component';
import {AddDeviceComponent} from './components/addDevice/add-device.component';
import {LoginComponent} from './components/login/login.component';
import {AuthGuard} from './auth.guard';


const routes: Routes = [
  {path: 'device/:id', component: ManagementComponent, canActivate: [AuthGuard]},
  {path: 'addDevice', component: AddDeviceComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: '', component: TableComponent, canActivate: [AuthGuard]},
  {path: '**', redirectTo: ''}
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


