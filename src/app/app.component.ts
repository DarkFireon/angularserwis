import {Component} from '@angular/core';
import {Data, Router} from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(public router: Router) {
  }

  logout() {
    this.router.navigate(['/login']);
  }
}

export interface Device {
  name: string;
  category: string;
  added: Data;
  status: string;
  id: number;
}
export interface Parameter {
  name: string;
  value: string;
  id: number;
}

export interface Comment {
  comment: string;
  createdOn: Date;
  id: number;
}

export interface DeviceInfo {
  name: string;
  category: string;
  added: Data;
  status: string;
  id: number;
  parameters: Array<Parameter>;
  comments: Array<Comment>;
}
