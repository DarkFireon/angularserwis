import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Device, DeviceInfo} from '../app.component';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  getParams(): Observable<Array<string>> {
    return this.http.get<Array<string>>('http://localhost:9000/parameters');
  }

  getDevice( url: string ): Observable<DeviceInfo> {
    return  this.http.get<DeviceInfo>( 'http://localhost:9000' + url);
  }
  getDevices(): Observable<Array<Device>> {
    return this.http.get<Array<Device>>('http://localhost:9000/devices');
  }

  getCategories(): Observable<Array<string>> {
    return  this.http.get<Array<string>>('http://localhost:9000/categories');
  }

  searchDeviceByName(pattern: string): Observable<Array<Device>> {
    return  this.http.get<Array<Device>>('http://localhost:9000/devices/search/name/' + pattern);
  }
  repairDevice(id: number): Observable<Device> {
    return this.http.get<Device>('http://localhost:9000/device/repair/' + id);
  }
  breakDevice(id: number): Observable<Device> {
    return  this.http.get<Device>('http://localhost:9000/device/break/' + id);
  }
  addCommentToDevice(deviceId: number, comment: string) {
    const parms = new HttpParams().set('comment', comment);
    return this.http.post('http://localhost:9000/comment/' + deviceId, {} , {params: parms} );
  }
  deleteComment(id: number): Observable<boolean> {
    return this.http.delete<boolean>('http://localhost:9000/comment/' + id);
  }

  deleteParamFromDevice(deviceId: number, paramId: number) {
    const parms = new HttpParams().set('deviceId', deviceId + '').append('paramId', paramId + '');
    return this.http.delete<boolean>('http://localhost:9000/device/deleteParam', {params: parms});
  }

  addParam(deviceId: number, param: string, value: string): Observable<boolean> {
     const parms = new HttpParams().set('parameter', param + '').append('value', value + '').append('deviceId', deviceId + '');
     return this.http.post<boolean>('http://localhost:9000/device/addParam', {}, {params: parms});
  }

  addDevice(name: string, category: string): Observable<Device> {
    const parms = new HttpParams().set('name', name).append('category', category);
    return this.http.post<Device>('http://localhost:9000/device', {} , {params: parms} );
  }
}
