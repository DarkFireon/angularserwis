import {Component} from '@angular/core';
import {HttpService} from '../../services/Http.service';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {AuthenticationService} from '../../services/authentication.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor(private httpService: HttpService, public router: Router, private authenticationService: AuthenticationService) {
    // reset login status
    this.authenticationService.logout();
  }

  error;
  username;
  password;

  onSubmit() {
    this.authenticationService.login(this.username, this.password)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['/']);
        },
        error => {
          this.error = 'Błedne hasło lub nazwa użytkownika';
        });
  }
}
