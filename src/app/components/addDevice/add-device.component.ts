import {Component, OnInit} from '@angular/core';
import {HttpService} from '../../services/Http.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-device',
  templateUrl: './add-device.component.html',
  styleUrls: ['./add-device.component.css']
})
export class AddDeviceComponent implements OnInit {

  constructor(private httpService: HttpService, public router: Router) {
  }

  chosenCategory: string;
  name: string;
  categories: string[];

  ngOnInit(): void {
    this.getCategories();
  }

  getCategories() {
    this.httpService.getCategories().subscribe(categories => {
      this.categories = categories;
    });
  }

  addDevice(name: string, category: string) {
    this.httpService.addDevice(name, category).subscribe(device => {
      this.router.navigate(['/device/' + device.id]);
    });
  }
}
