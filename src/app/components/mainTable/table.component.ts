import {Component, OnInit} from '@angular/core';
import {HttpService} from '../../services/Http.service';
import {Router} from '@angular/router';
import {Device} from '../../app.component';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  constructor( private httpService: HttpService, public router: Router) {
  }
  devices: Device[];

  ngOnInit(): void {
    this.getDevices();
  }
  getDevices() {
    this.httpService.getDevices().subscribe(devices => {
      this.devices = devices;
    });
  }


  showDevice(id: any) {
    this.router.navigate(['/device/' + id]);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.httpService.searchDeviceByName(filterValue.trim().toLowerCase()).subscribe(devices => {
      this.devices = devices;
    });
    console.log(filterValue.trim().toLowerCase());
  }

  addDevice() {
    this.router.navigate(['/addDevice']);
  }
}


